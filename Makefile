#
# This Makefile requires GNU make.
#
# Do not make changes here.
# Use the included .mak files.
#

make_need := 3.81
ifeq "" "$(strip $(filter $(make_need), $(firstword $(sort $(make_need) $(MAKE_VERSION)))))"
fail := $(error Your make ($(MAKE_VERSION)) is too old. You need $(make_need) or newer)
endif

-include config.mak
include package/targets.mak

INSTALL := ./tools/install.sh

install: install-script install-service install-service-module install-service-instance-module install-configure
install-script: $(SCRIPT_TARGET:module/scandir@/configure/%=$(DESTDIR)$(module_directory)/scandir@/configure/%)
install-service: $(SERVICE_TARGET:service/%=$(DESTDIR)$(service_directory)/%)
install-service-module: $(MODULE_TARGET:module/scandir@/service/%=$(DESTDIR)$(module_directory)/scandir@/service/%)
install-service-instance-module: $(MODULE_INSTANCE_TARGET:module/scandir@/service@/%=$(DESTDIR)$(module_directory)/scandir@/service@/%)
install-configure: $(MODULE_CONFIGURE_TARGET:module/scandir@/configure/configure=$(DESTDIR)$(module_directory)/scandir@/configure/configure)


$(DESTDIR)$(module_directory)/scandir@/configure/%: module/scandir@/configure/%
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(module_directory)/scandir@/configure/configure: module/scandir@/configure/configure
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(module_directory)/scandir@/service/%: module/scandir@/service/%
	exec $(INSTALL) -D -m 644 $< $@

$(DESTDIR)$(module_directory)/scandir@/service@/%: module/scandir@/service@/%
	exec $(INSTALL) -D -m 644 $< $@



$(DESTDIR)$(service_directory)/%: service/%
	exec $(INSTALL) -D -m 644 $< $@
	sed -i -e 's,@VERSION@,$(version),' \
		-e "s,@LIVEDIR@,$(livedir)," \
		-e "s,@USER_CONF_DIR@,$(service_userconf)," \
		-e "s,@LOGGER@,$(LOGGER)," \
		-e "s,@USE_ENVIRONMENT@,$(USE_ENVIRONMENT)," \
		-e "s,@VERBOSITY_LEVEL@,$(VERBOSITY_LEVEL)," \
		-e "s,@NOTIFY@,$(NOTIFY)," $@

version:
	@echo $(version)

.PHONY: install version

.DELETE_ON_ERROR:
