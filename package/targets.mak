SCRIPT_TARGET :=

SERVICE_TARGET := service/scandir@
MODULE_TARGET := $(shell find module/scandir@/service -type f)
MODULE_INSTANCE_TARGET := $(shell find module/scandir@/service@ -type f)

MODULE_CONFIGURE_TARGET := module/scandir@/configure/configure
